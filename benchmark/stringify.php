<?php

use DragonCode\Benchmark\Benchmark;
use Faker\Factory;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

error_reporting(E_ALL);
@ini_set('display_errors', 1);

require dirname(__DIR__) . '/vendor/autoload.php';

$faker = Factory::create();
$io = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

$sentences = 10000;
$iterations = 100;

$data = $faker->sentences($sentences);
$len_serialize = strlen(serialize($data));
$len_json_encode = strlen(json_encode($data));
$len_var_export_encode = strlen(var_export($data, true));

$io->title('Benchmark serialize / json_encode / var_export');

$io->text('- ' . $sentences . ' sentences');
$io->text('- ' . $iterations . ' iterations');
$io->writeln('');
$io->text('- serialize length: ' . $len_serialize);
$io->text('- json_encode length: ' . $len_json_encode);
$io->text('- var_export length: ' . $len_var_export_encode);
$io->writeln('');

Benchmark::start()
	->iterations($iterations)
	->withoutData()
	->round(2)
	->compare([
	'serialize' => function () use ($data) {
		serialize($data);
	},
	'json_encode' => function () use ($data) {
		json_encode($data);
	},
	'var_export' => function () use ($data) {
		var_export($data, true);
	},
]);
