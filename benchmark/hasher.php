<?php

use DragonCode\Benchmark\Benchmark;
use Faker\Factory;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

error_reporting(E_ALL);
@ini_set('display_errors', 1);

require dirname(__DIR__) . '/vendor/autoload.php';

$faker = Factory::create();
$io = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

$sentences = 10000;
$iterations = 100;

$data = $faker->sentences($sentences);
$len_serialize = strlen(serialize($data));
$len_json_encode = strlen(json_encode($data));

$io->title('Benchmark hash (md5, xxh128, xxh3, crc32)');

$io->text('- ' . $sentences . ' sentences');
$io->text('- ' . $iterations . ' iterations');
$io->writeln('');
$io->text('- serialize length: ' . $len_serialize);
$io->text('- json_encode length: ' . $len_json_encode);
$io->writeln('');

$_data = serialize($data);

$io->section('32 bits');
$io->text('- crc32: ' . crc32($_data));
$io->text('- crc32 (hexa): ' . str_pad(dechex(crc32($_data)), 8, '0', STR_PAD_LEFT));
$io->text('- hash(crc32): ' . hash('crc32', $_data));
$io->text('- hash(crc32b): ' . hash('crc32b', $_data));
$io->text('- hash(crc32c): ' . hash('crc32c', $_data));
$io->text('- hash(xxh32): ' . hash('xxh32', $_data));
$io->text('- hash(murmur3a): ' . hash('murmur3a', $_data));
$io->writeln('');

Benchmark::start()
	->iterations($iterations)
	->withoutData()
	->round(2)
	->compare([
		'crc32' => function () use ($_data) {
			crc32($_data);
		},
		'crc32 (hexa)' => function () use ($_data) {
			sprintf('%x', sprintf('%x', str_pad(dechex(crc32($_data)), 8, '0', STR_PAD_LEFT)));
		},
		'hash(crc32)' => function () use ($_data) {
			hash('crc32', $_data);
		},
		'hash(crc32b)' => function () use ($_data) {
			hash('crc32b', $_data);
		},
		'hash(crc32c)' => function () use ($_data) {
			hash('crc32c', $_data);
		},
		'hash(xxh32)' => function () use ($_data) {
			hash('xxh32', $_data);
		},
		'hash(murmur3a)' => function () use ($_data) {
			hash('murmur3a', $_data);
		},
]);


$io->section('64 bits');
$io->text('- hash(xxh3): ' . hash('xxh3', $_data));
$io->text('- hash(xxh64): ' . hash('xxh64', $_data));
$io->writeln('');

Benchmark::start()
	->iterations($iterations)
	->withoutData()
	->round(2)
	->compare([
		'hash(xxh3)' => function () use ($_data) {
			hash('xxh3', $_data);
		},
		'hash(xxh64)' => function () use ($_data) {
			hash('xxh64', $_data);
		},
]);

$io->section('128 bits');
$io->text('- hash(xxh128): ' . hash('xxh128', $_data));
$io->text('- md5: ' . md5($_data));
$io->writeln('');

Benchmark::start()
	->iterations($iterations)
	->withoutData()
	->round(2)
	->compare([
		'xxh128' => function () use ($_data) {
			hash('xxh128', $_data);
		},
		'md5' => function () use ($_data) {
			md5($_data);
		},
]);

$io->section('256 bits');
$io->text('- hash(ripemd256): ' . hash('ripemd256', $_data));
$io->text('- hash(sha3-256): ' . hash('sha3-256', $_data));
$io->text('- hash(sha512/256): ' . hash('sha512/256', $_data));
$io->text('- hash(sha256): ' . hash('sha256', $_data));
$io->writeln('');

Benchmark::start()
	->iterations($iterations)
	->withoutData()
	->round(2)
	->compare([
		'hash(sha512/256)' => function () use ($_data) {
			hash('sha512/256', $_data);
		},
		'hash(sha3-256)' => function () use ($_data) {
			hash('sha3-256', $_data);
		},
		'hash(ripemd256)' => function () use ($_data) {
			hash('ripemd256', $_data);
		},
		'hash(sha256)' => function () use ($_data) {
			hash('sha256', $_data);
		},
]);


$io->section('512 bits');
$io->text('- hash(sha512): ' . hash('sha512', $_data));
$io->text('- hash(sha3-512): ' . hash('sha3-512', $_data));
$io->writeln('');

Benchmark::start()
	->iterations($iterations)
	->withoutData()
	->round(2)
	->compare([
		'hash(sha512)' => function () use ($_data) {
			hash('sha512', $_data);
		},
		'hash(sha3-512)' => function () use ($_data) {
			hash('sha3-512', $_data);
		},
]);

$io->section('ALL');

Benchmark::start()
	->iterations($iterations)
	->withoutData()
	->round(2)
	->compare([
		'crc32 (hexa)' => function () use ($_data) {
			sprintf('%x', sprintf('%x', str_pad(dechex(crc32($_data)), 8, '0', STR_PAD_LEFT)));
		},
		'hhx64' => function () use ($_data) {
			hash('xxh64', $_data);
		},
		'hhx3' => function () use ($_data) {
			hash('xxh3', $_data);
		},
		'xxh128' => function () use ($_data) {
			hash('xxh128', $_data);
		},
		'sha512/256' => function () use ($_data) {
			hash('sha512/256', $_data);
		},
		'sha512' => function () use ($_data) {
			hash('sha512', $_data);
		},
		'md5' => function () use ($_data) {
			md5($_data);
		},
	]);

$io->section('ALL (serialize) + md5 json_encode');

Benchmark::start()
	->iterations($iterations)
	->withoutData()
	->round(2)
	->compare([
		'hash ser + crc32 (hexa)' => function () use ($data) {
			sprintf('%x', sprintf('%x', str_pad(dechex(crc32(serialize($data))), 8, '0', STR_PAD_LEFT)));
		},
		'hash ser + hhx64' => function () use ($data) {
			hash('xxh64', serialize($data));
		},
		'hash ser + hhx3' => function () use ($data) {
			hash('xxh3', serialize($data));
		},
		'hash ser + xxh128' => function () use ($data) {
			hash('xxh128', serialize($data));
		},
		'hash ser + sha512/256' => function () use ($data) {
			hash('sha512/256', serialize($data));
		},
		'hash ser + sha512' => function () use ($data) {
			hash('sha512', serialize($data));
		},
		'hash ser + md5' => function () use ($data) {
			md5(serialize($data));
		},
		'hash json + md5' => function () use ($data) {
			md5(json_encode($data));
		},
	]);
