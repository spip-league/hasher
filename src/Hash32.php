<?php

namespace SpipLeague\Component\Hasher;

use function crc32;
use function dechex;
use function str_pad;

class Hash32 implements HashInterface
{
    use StringifyTrait;

    /**
     * Returns 32 bit hash
     *
     * @note
     *    like hash('crc32b', $data) (crc32 in hexadecimal), but faster
     */
    public function hash(mixed $data): string
    {
        return str_pad(dechex(crc32($this->stringify($data))), 8, '0', \STR_PAD_LEFT);
    }
}
