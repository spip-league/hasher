<?php

namespace SpipLeague\Component\Hasher;

use function hash;

class Hash128 implements HashInterface
{
    use StringifyTrait;

    /**
     * Returns 128 bits hash (like md5)
     */
    public function hash(mixed $data): string
    {
        return hash('xxh128', $this->stringify($data));
    }
}
