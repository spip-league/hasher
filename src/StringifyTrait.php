<?php

namespace SpipLeague\Component\Hasher;

use function is_string;
use function serialize;

/**
 * Generate string for mixed data
 */
trait StringifyTrait
{
    private function stringify(mixed $data): string
    {
        if (is_string($data)) {
            return $data;
        }

        return serialize($data);
    }
}
