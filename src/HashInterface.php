<?php

namespace SpipLeague\Component\Hasher;

/**
 * Generate Hash for a cache usage
 *
 * This is not a cryptographic hash.
 * Output can differ in future version (we try to use the speedest algorithm)
 *
 * Functions returns hexadecimal string
 */
interface HashInterface
{
    public function hash(mixed $data): string;
}
