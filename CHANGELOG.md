# Changelog

## Unreleased

## 2.0.0 - 2024-07-18

### Changed

- namespace PHP de `Spip` à `SpipLeague`
- PHP 8.2 mini
