<?php

namespace SpipLeague\Test\Component\Hasher;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use SpipLeague\Component\Hasher\Hash128;
use SpipLeague\Component\Hasher\Hash32;
use SpipLeague\Component\Hasher\HashInterface;

#[CoversClass(Hash32::class)]
#[CoversClass(Hash128::class)]
class HashTest extends TestCase
{
    public function testHash32(): void
    {
        $hasher = new Hash32();
        $this->doHashTest($hasher, 8);
    }

    public function testHash128(): void
    {
        $hasher = new Hash128();
        $this->doHashTest($hasher, 32);
    }

    private function doHashTest(HashInterface $hasher, int $len): void
    {
        $hash = $hasher->hash('0');
        $this->assertSame($len, strlen($hash));
        $this->assertSame($hash, $hasher->hash('0'));
        $this->assertNotEquals($hash, $hasher->hash('00'));

        $this->assertSame($hasher->hash([0 => 1]), $hasher->hash([0 => 1]));
        $this->assertSame($hasher->hash(null), $hasher->hash(null));
        $this->assertSame(
            $hasher->hash(new \ArrayObject(['one' => 'two'])),
            $hasher->hash(new \ArrayObject(['one' => 'two'])),
        );
    }
}
