# Hash interface

Generates hexadecimal hash string (for a cache or key usage)

- 32 bits = 8 octets
- 128 bits = 32 octets

This is not a cryptographic hash.
Output can differ in future version as we try to use the fastest algorithm

## Installation

```bash
composer require spip-league/hasher
```

## Usage

```php
use SpipLeague\Component\Hasher\Hash32;

$hasher = new Hash32();
$hash = $hasher->hash($data);
```

## Interface

Provides a `HashInterface` with `hash(mixed $data): string` method.

```php
use SpipLeague\Component\Hasher\Hash128;
use SpipLeague\Component\Hasher\Hash32;
use SpipLeague\Component\Hasher\HashInterface;

function lambda(HashInterface $hasher)
{
  // $data = [...]
  $hash = $hasher->hash($data);
}

lambda(new Hash32());
lambda(new Hash128());
```

## Tests

See Composer scripts in [composer.json](composer.json#L42)

```bash
composer test
composer test-coverage
```

## Benchmark

```bash
composer test-benchmark
```
